package com.pv.demo.slice;

import com.pv.datetimeseer.Config;
import com.pv.datetimeseer.SeerFilter;
import com.pv.datetimeseer.SuggestionRow;
import com.pv.demo.ResourceTable;
import ohos.agp.components.*;
import ohos.app.Context;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;


/**
 * @author p-v
 */

public class AwesomeAdapter extends BaseItemProvider {
    private Context context;

    private SeerFilter suggestionFilter;
    private List<SuggestionRow> suggestionRowList;

    public AwesomeAdapter(Context context) {
        this.context = context;

    }

    @Override
    public int getCount() {
        return suggestionRowList == null ? 0 : suggestionRowList.size();
    }

    @Nullable
    @Override
    public SuggestionRow getItem(int position) {
        return suggestionRowList == null ? null : suggestionRowList.get(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public TextFilter getFilter() {
        if (suggestionFilter == null) {
            Config config = new Config.ConfigBuilder()
                    .setTimeFormat12HoursWithMins("h:mm a")
                    .setTimeFormat12HoursWithoutMins("h a")
                    .build();

            suggestionFilter =  new SeerFilter(context, config);
            suggestionFilter.setOnSuggestionPublishListener(new SeerFilter.OnSuggestionPublishListener() {
                @Override
                public void onSuggestionPublish(List<SuggestionRow> suggestionList) {
                    AwesomeAdapter.this.suggestionRowList = suggestionList;
                    if (suggestionList != null) {
                        AwesomeAdapter.this.notifyDataChanged();
                    } else {
                        AwesomeAdapter.this.notifyDataInvalidated();
                    }
                }
            });
        }
        return suggestionFilter;
    }
    @Override
    public Component getComponent(int position, Component convertView,@NotNull ComponentContainer parent) {
        if (convertView == null) {
            convertView = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_suggestion_row, parent, false);
        }
        Text textView = (Text) convertView.findComponentById(ResourceTable.Id_suggestion_textView);
        SuggestionRow suggestion = getItem(position);
        if (suggestion != null) {
            textView.setText(suggestion.getDisplayValue());
        }
        return convertView;
    }

}
