/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pv.demo.slice;

import com.pv.datetimeseer.SuggestionRow;
import com.pv.demo.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static ohos.agp.components.Component.HIDE;
import static ohos.agp.components.Component.VISIBLE;

public class MainAbilitySlice extends AbilitySlice {
    private ListContainer listContainer;
    private TextField search;
    private AwesomeAdapter awesomeAdapter;
    private ToastDialog toastDialog;


    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        listContainer = (ListContainer) findComponentById(ResourceTable.Id_list_container);

        search = (TextField) findComponentById(ResourceTable.Id_awesome_view);

        listContainer.setItemClickedListener( new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int i, long l) {
                if (awesomeAdapter != null) {
                    SuggestionRow suggestionRow = awesomeAdapter.getItem(i);
                    if (suggestionRow != null) {
                        String displayText;
                        if (suggestionRow.getValue() == SuggestionRow.PARTIAL_VALUE) {
                            displayText = suggestionRow.getDisplayValue() + " ";
                        } else {
                            displayText = suggestionRow.getDisplayValue();
                            long selectedTime = suggestionRow.getValue() * 1000L;
                            DateFormat df = new SimpleDateFormat("EEEE, d MMMM yyyy h:mma", Locale.ENGLISH);
                            String timeOnScreen = df.format(new Date(selectedTime));
                            showToast(String.format(getString(ResourceTable.String_awesome_time),
                                    timeOnScreen));
                        }
                        search.setText(displayText);

                    }
                }
            }
        });

        search.addTextObserver(new Text.TextObserver() {
            @Override
            public void onTextUpdated(String s, int i, int i1, int i2) {
                String searchedquery = s.trim();
                if(searchedquery.length() > 0){
                    //The line below searches the listContainer and shows the results
                    awesomeAdapter.getFilter().filter(searchedquery);
                    if (awesomeAdapter.getCount() > 0)
                        listContainer.setVisibility(VISIBLE);
                    else
                        listContainer.setVisibility(HIDE);
                }

            }

        });
        awesomeAdapter = new AwesomeAdapter(this);

        listContainer.setItemProvider(awesomeAdapter);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    private void showToast(String message) {
        toastDialog = new ToastDialog(getContext());
        Component component = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_toast_layout, null, false);
        Text text = (Text) component.findComponentById(ResourceTable.Id_toast_text);
        text.setText(message);
        toastDialog.setDuration(500);
        toastDialog.setComponent(component);
        toastDialog.setSize(ComponentContainer.LayoutConfig.MATCH_CONTENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
        toastDialog.setAlignment(LayoutAlignment.CENTER | LayoutAlignment.HORIZONTAL_CENTER);
        toastDialog.show();
    }

}
