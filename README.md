# DateTimeSeer

A painless way to pick future time
DateTimeSeer is an openharmony seer who gets visions of the date and time you are thinking.
It tells you what you might be thinking and helps in what modern people call as autocompletion. Unfortunately, he currently knows only english.


# Usage Instructions
The library exposes SeerFilter which extends TextFilter class and so can be hooked to anything which extends BaseItemProvider.
Use the ConfigBuilder to provide Date/Time formats.
The sample app here demonstrate the usage of the TextFilter with the TextField.
A sample project which provides runnable code examples that demonstrate uses of the classes in this project is available in the entry/ folder.

The following core classes are the essential interface to DateTimeSeer:
DateTimeSeer - Hepls to add suggestionRowList to ListContainers

Setting ItemProvider for ListContainer:
	Create Adapter with BaseItem provider of ListContainer and call setItemProvider of the ListContainer
```
awesomeAdapter = new Adapter(this);
listContainer.setItemProvider(awesomeAdapter);
```

The steps to add suggestionRowList:
Note: Use any layout that you want to set suggestionRowList to ListContainer.
```
suggestionRowList.get(position)
suggestion.getDisplayValue()
```
With TextObserver interface Filter the ListContainer when Text is updated in TextField
```
search.addTextObserver(new Text.TextObserver() {

			@Override
			public void onTextUpdated(String s, int i, int i1, int i2) {
					awesomeAdapter.getFilter().filter(s);
			}

		});
```
Please refer the sample app for implementation


# Installation Instructions
DateTimeSeer is dependent on 'org.jetbrains:annotations:15.0'

Solution 1: For using DateTimeSeer module in your sample application.

	Modify entry build.gradle as below :
	```
	dependencies {
  		 implementation fileTree(dir: 'libs', include:['*.jar', '*.har'])
   		implementation project(':library)
   		implementation 'org.jetbrains:annotations:15.0'
 }
	```
	
Solution 2: For using DateTimeSeer in separate application, add the below dependencies and include " DateTimeSeer.har" in libs folder of "entry" module  :
	```
	dependencies {
		implementation fileTree(dir: 'libs', include: ['*.har'])
		implementation 'org.jetbrains:annotations:15.0'
	}
    ```
 
Solution 3: For using DateTimeSeer from a remote repository in separate application, add the below dependency in entry/build.gradle file.
 
 Modify entry build.gradle as below :
 ```gradle
 dependencies {
        implementation 'io.openharmony.tpc.thirdlib:DateTimeSeer:1.0.0'
 }
 ```
